<?php
    session_start();

    if(empty($_POST['phone']) && empty($_POST['amount'])){
        echo " <br/> Please fill in the fields";
    }else{
            $phone          = $_POST['phone'];
            $amount         = $_POST['amount'];
            $acc_no         = 'mpesa';
            
            if (substr($phone, 0, 1) == "0") { 
                $phone = "254" . substr($phone, -9);
            }

            $url1 = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url1);
            $credentials = base64_encode('bZfGmoxMDqztwDoGRhn5GFkxbuz7xdKA:e3Kp1BKeTm05DGpW');
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
            curl_setopt($curl, CURLOPT_HEADER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curl_response = curl_exec($curl);
            //echo $curl_response;
            $curl_response = explode('"access_token": ', $curl_response);
            $curl_response1 = explode('"expires_in":', $curl_response[1]);
            $trimmed  = rtrim($curl_response1[0], ',');
            $trimmed1 = trim($trimmed, '"');
            $curl_response2 = explode('"', $trimmed1);
            $curl_response3 = explode('"', $curl_response2[0]);
            $token = $curl_response3[0];

            $MERCHANT_ID =    '4079611';
            $PASSKEY = '636902c892ae90629582906138f362b00c83ecd5be7ebeeaf934d33c91f487d8';
            $TIMESTAMP = date("YmdHis", time());
            //openssl_public_encrypt($plaintext, $encrypted, $publicKey, OPENSSL_PKCS1_PADDING);
            $password = base64_encode($MERCHANT_ID . $PASSKEY . $TIMESTAMP);

            $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token.'')); //setting custom header
            
            $curl_post_data = array(
            //Fill in the request parameters with valid values
            'BusinessShortCode' => $MERCHANT_ID,
            'Password' => $password,
            'Timestamp' => $TIMESTAMP,
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => $amount,
            'PartyA' => $phone,
            'PartyB' => $MERCHANT_ID,
            'PhoneNumber' => $phone,
            'CallBackURL' => 'https://dukayetuonline.co.ke/make-payment-stkcallback',
            'AccountReference' => $acc_no,
            'TransactionDesc' => $acc_no,
            );

            $data_string = json_encode($curl_post_data);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

            $response = curl_exec($curl);
            $response_array = json_decode($response);

            // if(isset($response_array->CheckoutRequestID) && $response_array->ResponseCode == 0){
            //     $sql  ="INSERT INTO `transactions` ( `user_id`, `msisdn`, `checkoutRequestID`, `amount`, `status`)  VALUES ('$user_id', '$phone', '$response_array->CheckoutRequestID', '$amount', '0');";
            //     $db->sql($sql);
            //     $res = $db->getResult();
            // }

            print_r(json_encode($response_array));
            return false;
    }     
 
?>