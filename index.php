<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Mpesa</title>
    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        .sign_in {
            margin-top: 10%;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div style="text-align:center; margin-top: 110px;">
                    <img src="images/mpesa.png" width="50%" height=50%" /></div>
			        <div class="login-panel panel panel-default ">
                    <div class="panel-heading ">
                        <h3 class="panel-title "></h3>
                    </div>
                    <div class="panel-body ">
                    <form action="mpesa.php" method="post">
                            <font color="#FF0000 "></font>
                        <label>Phone Number</label>
                        <input name="phone" type="tel"  class="form-control" placeholder="Enter Phone Number Here... " required/>
                        <label>Amount</label>
                        <input name="amount"  type="tel" class="form-control" placeholder="Enter Amount Here... " required/>
                        <br/>
                        <button type="submit" name="submit" class="btn btn-success btn-block ">Submit</button>
                        <br>
                    </form>
                    <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js "></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js "></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js "></script>
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js "></script>
</body>
</html>